#! /usr/bin/python3
import logging
import pytest
from bitcoin.rpc import JSONRPCError

def test_wormups_bitcoin_rpc(bitcoind):
    """
    Wormup with bitcoin rpc
    """
    info = bitcoind.rpc.getblockchaininfo()
    logging.warning(info)
    assert info is not None

def test_load_new_wallet(bitcoind):
    addr = bitcoind.rpc.getnewaddress()
    list_hash = bitcoind.rpc.generatetoaddress(10, addr)
    assert list_hash is not None

    bitcoind.rpc.createwallet("api")
    with pytest.raises(JSONRPCError):
        bitcoind.rpc.loadwallet("api")
        ## Raise because we need to change the url

def test_with_def_wallet(bitcoind):
    """
    Create different wallet, and test if there is any error
    """
    addr = bitcoind.rpc.getnewaddress()
    list_hash = bitcoind.rpc.generatetoaddress(10, addr)
    assert list_hash is not None


def test_create_different_wallet(bitcoind):
    """
    Create different wallet, and test if there is any error
    """
    bitcoind.rpc.createwallet("tests")
    addr = bitcoind.rpc.getnewaddress()
    list_hash = bitcoind.rpc.generatetoaddress(10, addr)
    assert list_hash is not None

def test_stress_create_different_wallet(bitcoind):
    """
    Create different wallet, and test if there is any error
    """
    bitcoind.rpc.createwallet("tests-1")
    addr = bitcoind.rpc.getnewaddress()
    list_hash = bitcoind.rpc.generatetoaddress(5, addr)
    assert list_hash is not None

    bitcoind.rpc.createwallet("tests-2")
    addr = bitcoind.rpc.getnewaddress()
    list_hash = bitcoind.rpc.generatetoaddress(5, addr)
    assert list_hash is not None

    bitcoind.rpc.createwallet("tests-3")
    addr = bitcoind.rpc.getnewaddress()
    list_hash = bitcoind.rpc.generatetoaddress(5, addr)
    assert list_hash is not None
